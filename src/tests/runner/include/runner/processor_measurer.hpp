#pragma once

#include "measure_utils.hpp"

#include <ostream>

template <typename PROCESSOR_TYPE>
struct PROCESSOR_MEASURER
{
	PROCESSOR_MEASURER(PROCESSOR_TYPE aProcessor, std::ostream& aOutput) :
		mProcessor{ std::move(aProcessor) },
		mOutput{ aOutput }
	{}
	
	void RunTest(const typename PROCESSOR_TYPE::INPUT& aInput)
	{
		const auto Functor = [&](const typename PROCESSOR_TYPE::INPUT& aData){ return mProcessor.Process(aData); };
		const auto Result = MultipleMeasureFunction<5>(Functor, aInput);
		
		mOutput << "Function took " << Result.count() << "ms to run." << std::endl;
	}
	
private:
	PROCESSOR_TYPE mProcessor;
	std::ostream& mOutput;
};
