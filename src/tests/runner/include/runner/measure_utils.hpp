#pragma once

#include <chrono>
#include <thread>
#include <functional>

template<typename RETURN_TYPE, typename ... ARGUMENTS>
using MY_FUNCTION = RETURN_TYPE(*)(ARGUMENTS...);

template<typename FUNCTION_TYPE, typename ... ARGUMENTS, typename RESULT_TYPE = typename std::result_of<FUNCTION_TYPE(ARGUMENTS...)>::type>
std::chrono::milliseconds MeasureFunction(FUNCTION_TYPE&& aFunction, ARGUMENTS&&... aArgs)
{
	const auto Start = std::chrono::high_resolution_clock::now();
	
	aFunction(std::forward<ARGUMENTS>(aArgs)...);
	
	const auto End = std::chrono::high_resolution_clock::now();
	
	return std::chrono::duration_cast<std::chrono::milliseconds>(End - Start);
};

template<uint8_t COUNT, typename FUNCTION_TYPE, typename ... ARGUMENTS, typename RESULT_TYPE = typename std::result_of<FUNCTION_TYPE(ARGUMENTS...)>::type>
std::chrono::milliseconds MultipleMeasureFunction(FUNCTION_TYPE&& aFunction, ARGUMENTS&&... aArgs)
{
	auto Result = std::chrono::milliseconds(0);
	for(uint8_t I = 0; I < COUNT; ++I)
	{
		Result += MeasureFunction(aFunction, std::forward<ARGUMENTS>(aArgs)...);
	}
	return Result / COUNT;
}
