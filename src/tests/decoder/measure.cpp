#include <decoder/decoder.h>
#include <runner/processor_measurer.hpp>
#include <std_port/index_sequence.hpp>

#include <iostream>
#include <numeric>

constexpr bool IsPrimeLoop(uint32_t aNumber, uint32_t aDivider)
{
	return (aDivider * aDivider > aNumber) ?
				true :
				(aNumber % aDivider == 0) ?
					false :
					IsPrimeLoop(aNumber, aDivider + 1);
}

constexpr bool IsPrime(uint32_t aNumber)
{
	return IsPrimeLoop(aNumber, 2);
}

constexpr uint32_t NextPrime(uint32_t aNumber)
{
	return IsPrime(aNumber) ? aNumber : NextPrime(aNumber + 1);
}

constexpr int GetPrimeLoop(uint32_t aIndex, int aPrime)
{
	return (aIndex == 0) ?
				aPrime :
				(aIndex % 2) ?
					GetPrimeLoop(aIndex - 1, NextPrime(aPrime + 1)) :
					GetPrimeLoop(aIndex / 2, GetPrimeLoop(aIndex / 2, aPrime));
}

constexpr uint32_t GetPrime(uint32_t aIndex)
{
	return GetPrimeLoop(aIndex, 2);
}

#ifdef MSVC

template<std::size_t COUNT>
constexpr std::array<uint32_t, COUNT> GetPrimes()
{
	std::array<uint32_t, COUNT> Primes{};
	for (auto Index = 0; Index < COUNT; ++Index)
	{
		Primes[Index] = GetPrime(Index);
	}
	return Primes;
}

#else

template<size_t... INDICIES>
constexpr std::array<uint32_t, sizeof...(INDICIES)> GetPrimes(std_port::index_sequence<INDICIES...>)
{
	return { GetPrime(INDICIES)... };
}

template<std::size_t COUNT>
constexpr std::array<uint32_t, COUNT> GetPrimes()
{
	return GetPrimes(std_port::make_index_sequence<COUNT>{});
}

#endif // MSVC

int main()
{
	DECODER Instance;
	PROCESSOR_MEASURER<DECODER> Measurer(Instance, std::cout);
	
	const auto Numbers = GetPrimes<10000>();
	std::vector<uint32_t> MoreNumbers;
	for(uint32_t Counter = 0; Counter < 100; ++Counter)
	{
		std::copy(Numbers.begin(), Numbers.end(), std::back_inserter(MoreNumbers));
	}
	
	DECODER::INPUT BigInput{100, {MoreNumbers.begin(), MoreNumbers.end()}};
	
	Measurer.RunTest(BigInput);
	
	return 0;
}
