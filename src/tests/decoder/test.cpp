#include <decoder/decoder.h>
#include <gtest/gtest.h>
#include <array>

using TEST_PAIR = std::pair<DECODER::INPUT, DECODER::OUTPUT>;

struct DECODER_TEST : ::testing::TestWithParam<TEST_PAIR>
{
	DECODER mProcessor;
};

TEST_P(DECODER_TEST, Parametrized)
{
	const auto Params = GetParam();
	const auto Input = Params.first;
	const auto Output = Params.second;
	
	EXPECT_EQ(mProcessor.Process(Input), Output);
}

const DECODER::INPUT EmptyInput = {};
const DECODER::OUTPUT EmptyOutput = {};

const DECODER::INPUT NullInput = {{0}};
const DECODER::OUTPUT NullOutput = {{{{0, 0, 0}}}};

const DECODER::INPUT PixelInput = {{360797}};
const DECODER::OUTPUT PixelOutput = {{{{93, 129, 5}}}};

const DECODER::INPUT SmallInput = {{0, 16777215}, {16777215, -83886075}};
const DECODER::OUTPUT SmallOutput = {{{{0, 0, 0}}, {{255, 255, 255}}}, {{{255, 255, 255}}, {{0, 0, 0}}}};

const DECODER::INPUT NormalInput = {{0, 65536, 0, -65536, 720896}, {655360, -3080192, 9109504, -21168128, 42336256}};
const DECODER::OUTPUT NormalOutput = {{{{0, 0, 0}}, {{0, 0, 1}}, {{0, 0, 5}}, {{0, 0, 9}}, {{0, 0, 16}}}, {{{0, 0, 10}}, {{0, 0, 3}}, {{0, 0, 4}}, {{0, 0, 2}}, {{0, 0, 1}}}};

const DECODER::INPUT BigInput =
{
	{13964981, -59578163, 171290943, -396947953, 800080280, -1445966850, -1892023694, 562426681, 1200312456, 848140914},
	{12939157, -54173110, 146347843, -310472328, 572384289, -948652255, 1462889357, -2137965900, -1293536336, 209458871},
	{15636144, -64826752, 168824382, -349818874, 644539991, -1089642841, 1735593410, 1663311065, -468832573, -1063240423},
	{11784891, -43957917, 108047402, -209576423, 348484585, -515588376, 702004714, -894085542, 1074264702, -1207994716},
	{14775128, -64186920, 173489713, -364108291, 668336630, -1125328600, 1781825570, 1609157829, -405037950, -1154222206},
	{1758361, 5652863, -39210600, 137373621, -356441756, 772553332, -1475059479, -1727400787, 140120105, 2054293739}
};
const DECODER::OUTPUT BigOutput =
{
	{{{181, 22, 213}}, {{86, 90, 156}}, {{82, 32, 199}}, {{94, 127, 51}}, {{228, 253, 34}}, {{184, 175, 212}}, {{246, 249, 19}}, {{217, 126, 150}}, {{12, 119, 88}}, {{86, 131, 135}}},
	{{{149, 111, 197}}, {{51, 144, 160}}, {{135, 94, 74}}, {{125, 56, 136}}, {{228, 168, 98}}, {{27, 30, 215}}, {{197, 237, 248}}, {{164, 211, 118}}, {{29, 186, 82}}, {{119, 15, 0}}},
	{{{176, 150, 238}}, {{240, 195, 203}}, {{158, 13, 16}}, {{28, 147, 36}}, {{81, 143, 207}}, {{50, 0, 223}}, {{93, 224, 211}}, {{235, 17, 229}}, {{211, 170, 3}}, {{144, 144, 124}}},
	{{{187, 210, 179}}, {{10, 95, 228}}, {{103, 46, 93}}, {{23, 68, 136}}, {{143, 111, 6}}, {{189, 2, 54}}, {{187, 202, 143}}, {{189, 74, 77}}, {{26, 236, 12}}, {{51, 84, 199}}},
	{{{88, 115, 225}}, {{144, 214, 147}}, {{217, 172, 4}}, {{210, 184, 140}}, {{249, 72, 224}}, {{82, 45, 61}}, {{53, 26, 11}}, {{189, 216, 96}}, {{10, 73, 52}}, {{208, 70, 35}}},
	{{{153, 212, 26}}, {{124, 104, 220}}, {{13, 71, 101}}, {{157, 124, 235}}, {{208, 141, 55}}, {{126, 122, 30}}, {{158, 25, 101}}, {{203, 231, 15}}, {{213, 183, 199}}, {{60, 247, 97}}}
};

const std::array<TEST_PAIR, 6> TestData =
{{
	{EmptyInput, EmptyOutput},
	{NullInput, NullOutput},
	{PixelInput, PixelOutput},
	{SmallInput, SmallOutput},
	{NormalInput, NormalOutput},
	{BigInput, BigOutput}
}};

INSTANTIATE_TEST_CASE_P(, DECODER_TEST, ::testing::ValuesIn(TestData));

int main(int argc, char **argv)
{
	::testing::InitGoogleTest( &argc, argv );
	return RUN_ALL_TESTS();
}
