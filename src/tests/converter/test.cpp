#include <converter/converter.h>
#include <gtest/gtest.h>
#include <array>

using TEST_PAIR = std::pair<CONVERTER::INPUT, CONVERTER::OUTPUT>;

struct CONVERTER_TEST : ::testing::TestWithParam<TEST_PAIR>
{
	CONVERTER mProcessor;
};

TEST_P(CONVERTER_TEST, Parametrized)
{
	const auto Params = GetParam();
	const auto Input = Params.first;
	const auto Output = Params.second;
	
	EXPECT_EQ(mProcessor.Process(Input), Output);
}

const CONVERTER::INPUT SeaInput = {{{{0x00, 0x00, 0xFF}}}};
const CONVERTER::OUTPUT SeaOutput = {{FIELD::SEA}};

const CONVERTER::INPUT CoastInput = {{{{0x00, 0xFF, 0xFF}}}};
const CONVERTER::OUTPUT CoastOutput = {{FIELD::COAST}};

const CONVERTER::INPUT ShoreInput = {{{{0x7C, 0xFC, 0x00}}}};
const CONVERTER::OUTPUT ShoreOutput = {{FIELD::SHORE}};

const CONVERTER::INPUT ForestInput = {{{{0x22, 0x8B, 0x22}}}};
const CONVERTER::OUTPUT ForestOutput = {{FIELD::FOREST}};

const CONVERTER::INPUT DesertInput = {{{{0xF4, 0xA4, 0x60}}}};
const CONVERTER::OUTPUT DesertOutput = {{FIELD::DESERT}};

const CONVERTER::INPUT MountainInput = {{{{0x70, 0x80, 0x90}}}};
const CONVERTER::OUTPUT MountainOutput = {{FIELD::MOUNTAIN}};

const CONVERTER::INPUT MarshInput = {{{{0x8B, 0x45, 0x13}}}};
const CONVERTER::OUTPUT MarshOutput = {{FIELD::MARSH}};

const CONVERTER::INPUT HarborInput = {{{{0x00, 0x00, 0x00}}}};
const CONVERTER::OUTPUT HarborOutput = {{FIELD::PIRATE_HARBOR}};

const CONVERTER::INPUT TreasureInput = {{{{0xFF, 0x00, 0x00}}}};
const CONVERTER::OUTPUT TreasureOutput = {{FIELD::TREASURE}};

const CONVERTER::INPUT SmallInput = {{{{0, 0, 0}}, {{255, 255, 255}}}, {{{255, 255, 255}}, {{0, 0, 0}}}};
const CONVERTER::OUTPUT SmallOutput = {{FIELD::MOUNTAIN, FIELD::MOUNTAIN}, {FIELD::MOUNTAIN, FIELD::MOUNTAIN}};

const CONVERTER::INPUT NormalInput =
{
	{{{0, 0, 0}}, {{0, 0, 1}}, {{0, 0, 5}}, {{0, 0, 9}}, {{0, 0, 16}}},
	{{{0, 0, 10}}, {{0, 0, 3}}, {{0, 0, 4}}, {{0, 0, 2}}, {{0, 0, 1}}}
};
const CONVERTER::OUTPUT NormalOutput =
{
	{FIELD::PIRATE_HARBOR, FIELD::PIRATE_HARBOR, FIELD::PIRATE_HARBOR, FIELD::PIRATE_HARBOR, FIELD::PIRATE_HARBOR},
	{FIELD::PIRATE_HARBOR, FIELD::PIRATE_HARBOR, FIELD::PIRATE_HARBOR, FIELD::PIRATE_HARBOR, FIELD::PIRATE_HARBOR}
};

const CONVERTER::INPUT BigInput =
{
	{{{0x22, 0x8B, 0x22}}, {{0x22, 0x8B, 0x22}}, {{0x22, 0x8B, 0x22}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0x7C, 0xFC, 0x00}}, {{0x7C, 0xFC, 0x00}}, {{0x7C, 0xFC, 0x00}}},
	{{{0x70, 0x80, 0x90}}, {{0x70, 0x80, 0x90}}, {{0x70, 0x80, 0x90}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0x7C, 0xFC, 0x00}}, {{0x7C, 0xFC, 0x00}}},
	{{{0x70, 0x80, 0x90}}, {{0x70, 0x80, 0x90}}, {{0x70, 0x80, 0x90}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0x22, 0x8B, 0x22}}, {{0xFF, 0x00, 0x00}}, {{0xF4, 0xA4, 0x60}}, {{0xF4, 0xA4, 0x60}}},
	{{{0xF4, 0xA4, 0x60}}, {{0xF4, 0xA4, 0x60}}, {{0xF4, 0xA4, 0x60}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0xFF, 0x00, 0x00}}, {{0x7C, 0xFC, 0x00}}, {{0x7C, 0xFC, 0x00}}},
	{{{0x00, 0xFF, 0xFF}}, {{0x00, 0xFF, 0xFF}}, {{0x00, 0xFF, 0xFF}}, {{0x22, 0x8B, 0x22}}, {{0x8B, 0x45, 0x13}}, {{0x22, 0x8B, 0x22}}, {{0x8B, 0x45, 0x13}}, {{0xFF, 0x00, 0x00}}, {{0x00, 0x00, 0x00}}, {{0x00, 0x00, 0xFF}}},
	{{{0x22, 0x8B, 0x22}}, {{0x22, 0x8B, 0x22}}, {{0x22, 0x8B, 0x22}}, {{0xFF, 0x00, 0x00}}, {{0x8B, 0x45, 0x13}}, {{0x8B, 0x45, 0x13}}, {{0x8B, 0x45, 0x13}}, {{0xFF, 0x00, 0x00}}, {{0x7C, 0xFC, 0x00}}, {{0x00, 0x00, 0xFF}}}
};
const CONVERTER::OUTPUT BigOutput =
{
	{FIELD::FOREST, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::TREASURE, FIELD::TREASURE, FIELD::MARSH, FIELD::MARSH, FIELD::SHORE, FIELD::SHORE},
	{FIELD::MOUNTAIN, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::TREASURE, FIELD::TREASURE, FIELD::MARSH, FIELD::MARSH, FIELD::SHORE, FIELD::SHORE},
	{FIELD::MOUNTAIN, FIELD::MOUNTAIN, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::TREASURE, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH},
	{FIELD::MOUNTAIN, FIELD::MOUNTAIN, FIELD::MOUNTAIN, FIELD::MARSH, FIELD::MARSH, FIELD::TREASURE, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MOUNTAIN},
	{FIELD::MOUNTAIN, FIELD::MOUNTAIN, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MOUNTAIN},
	{FIELD::FOREST, FIELD::MOUNTAIN, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MARSH, FIELD::MOUNTAIN}
};

const std::array<TEST_PAIR, 12> TestData
{{
	{SeaInput, SeaOutput},
	{CoastInput, CoastOutput},
	{ShoreInput, ShoreOutput},
	{ForestInput, ForestOutput},
	{DesertInput, DesertOutput},
	{MountainInput, MountainOutput},
	{MarshInput, MarshOutput},
	{HarborInput, HarborOutput},
	{TreasureInput, TreasureOutput},
	{SmallInput, SmallOutput},
	{NormalInput, NormalOutput},
	{BigInput, BigOutput},
}};

INSTANTIATE_TEST_CASE_P(, CONVERTER_TEST, ::testing::ValuesIn(TestData));

int main(int argc, char **argv)
{
	::testing::InitGoogleTest( &argc, argv );
	return RUN_ALL_TESTS();
}
