#include <converter/converter.h>
#include <runner/processor_measurer.hpp>
#include <std_port/index_sequence.hpp>

#include <iostream>
#include <numeric>

constexpr PIXEL GetPixel(uint32_t aNumber)
{
	return {static_cast<uint8_t>(aNumber << 4), static_cast<uint8_t>(128 - aNumber), static_cast<uint8_t>(aNumber >> 4)};
}

#ifdef MSVC

template<std::size_t COUNT>
constexpr std::array<PIXEL, COUNT> GetPixels()
{
	std::array<PIXEL, COUNT> Pixels{};
	
	for (auto Index = 0; Index < COUNT; ++Index)
	{
		Pixels[Index] = GetPixel(Index);
	}

	return Pixels;
}

#else

template<size_t... INDICIES>
constexpr std::array<PIXEL, sizeof...(INDICIES)> GetPixels(std_port::index_sequence<INDICIES...>)
{
	return {GetPixel(INDICIES)...};
}

template<std::size_t COUNT>
constexpr std::array<PIXEL, COUNT> GetPixels()
{
	return GetPixels(std_port::make_index_sequence<COUNT>{});
}

#endif

int main()
{
	const auto Pixels = GetPixels<2000>();
	CONVERTER::INPUT BigInput{1000, {Pixels.begin(), Pixels.end()}};
	
	CONVERTER Instance;
	PROCESSOR_MEASURER<CONVERTER> Measurer(Instance, std::cout);
	
	Measurer.RunTest(BigInput);
	
	return 0;
}