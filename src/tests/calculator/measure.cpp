#include <calculator/calculator.h>
#include <runner/processor_measurer.hpp>

#include <iostream>
#include <random>

int main()
{
	CALCULATOR::INPUT BigInput{};
	uint32_t StepSize = 256;
	for (uint32_t RowIndex = 0; RowIndex < 1024; ++RowIndex)
	{
		std::vector<FIELD> Row;
		for (uint32_t ColIndex = 0; ColIndex < 1024; ++ColIndex)
		{
			Row.push_back(RowIndex % StepSize == 0 && ColIndex % StepSize == 0 ? FIELD::PIRATE_HARBOR : FIELD::SEA);
		}
		BigInput.push_back(Row);
	}

	BigInput[0][0] = FIELD::TREASURE;

	CALCULATOR Instance;
	PROCESSOR_MEASURER<CALCULATOR> Measurer(Instance, std::cout);

	Measurer.RunTest(BigInput);

	return 0;
}
