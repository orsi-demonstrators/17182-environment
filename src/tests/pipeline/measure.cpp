#include <pipeline/pipeline.h>
#include <core/types/picture.h>
#include <runner/processor_measurer.hpp>
#include <algorithm>

void AddPixels(PICTURE& aMap, std::pair<uint32_t, uint32_t> aSize, 
	std::pair<uint32_t, uint32_t> aKernel, const PIXEL aPixel)
{
	uint32_t Row, Col;
	std::tie(Row, Col) = aSize;
	uint32_t RowKernel = aKernel.first / 2;
	uint32_t ColKernel = aKernel.second / 2;
	for (uint32_t RowIndex = std::max(0u, Row - RowKernel); 
		 RowIndex <= std::min(static_cast<uint32_t>(aMap.size()) - 1, Row + RowKernel); 
		 ++RowIndex)
	{
		for (uint32_t ColIndex = std::max(0u, Col - ColKernel); 
			 ColIndex <= std::min(static_cast<uint32_t>(aMap[RowIndex].size()) - 1, Col + ColKernel); 
			 ++ColIndex)
		{
			aMap[RowIndex][ColIndex] = aPixel;
		}
	}
}

PICTURE CreateMap(uint32_t aRows, uint32_t aCols, uint32_t aStepSize, const PIXEL aDefault)
{
	PICTURE Map(aRows, std::vector<PIXEL>(aCols, aDefault));

	auto AddHorizontalPirate = [](PICTURE& aMap, uint32_t aRow, uint32_t aCol) {
		return AddPixels(aMap, { aRow, aCol }, { 5, 3 }, { 0, 0, 0 });
	};
	for (uint32_t PirateIndex = 0; PirateIndex <= Map[0].size() / aStepSize; ++PirateIndex)
	{
		AddHorizontalPirate(Map, 0, PirateIndex * aStepSize);
		AddHorizontalPirate(Map, Map[0].size() - 1, PirateIndex * aStepSize);
	}

	auto AddVerticalPirate = [](PICTURE& aMap, uint32_t aRow, uint32_t aCol) {
		return AddPixels(aMap, { aRow, aCol }, { 3, 5 }, { 0, 0, 0 });
	};
	for (uint32_t PirateIndex = 1; PirateIndex < Map.size() / aStepSize; ++PirateIndex)
	{
		AddVerticalPirate(Map, PirateIndex * aStepSize, 0);
		AddVerticalPirate(Map, PirateIndex * aStepSize, Map.size() - 1);
	}

	AddPixels(Map, { Map.size() / 2, Map[0].size() }, { 5, 5 }, { 255, 0, 0 });

	return Map;
}

COMPRESSED_PICTURE EncodeMap(const PICTURE& aMap)
{
	COMPRESSED_PICTURE Encoded(aMap.size(), std::vector<COMPRESSED_PIXEL>(aMap[0].size()));

	for (uint32_t RowIndex = 0; RowIndex < aMap.size(); ++RowIndex)
	{	
		std::transform(aMap[RowIndex].begin(), aMap[RowIndex].end(), Encoded[RowIndex].begin(), [](const PIXEL& aPixel) {
			return (aPixel[2] << 16) + (aPixel[1] << 8) + aPixel[0];
		});

		for (uint32_t N = 0; N < 5; ++N)
		{
			for (uint32_t ColIndex = 1; ColIndex < aMap[RowIndex].size(); ++ColIndex)
			{
				Encoded[RowIndex][ColIndex] -= Encoded[RowIndex][ColIndex - 1];
			}
		}
	}

	return Encoded;
}

int main()
{
	PIPELINE::INPUT Inputs(100, EncodeMap(CreateMap(64, 64, 32, { 185, 122, 87 })));

	PIPELINE Instance;
	PROCESSOR_MEASURER<PIPELINE> Measurer(Instance, std::cout);

	Measurer.RunTest(Inputs);

	return 0;
}
