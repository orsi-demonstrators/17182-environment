## Történet
A kalózok az internet tengerén hajózva régi titkos térképekre bukkantak. Az ősi 
kalózok a térképeket titkosították, hogy azokat mások meg ne kaparinthassák. 
Szerencsére a titkosításhoz használt algoritmus benne volt a README-be, viszont 
a visszafejtéshez szükséges algoritmus hollétét elfelejtették dokumentálni. 
A te feladatod, hogy készíts egy programot, mely visszaalakít egy titkosított 
térképet használható formába. 
A kalózok többmagos gépeket zsákmányoltak az egyik kalandjuk során, így ahhoz, 
hogy minél több kincset tudjanak minél hamarabb szerezni, fontos, hogy a 
programod kihasználja a hardver lehetőségeit.

## Feladat
A tömörítő algoritmus a következő volt:
Minden képpontból színkomponensek felhasználásával készítettek egy 4 byte-os 
számot. ( Minden színkomponens maximum 1 byte-on van ábrázolva. ) Ehhez a kék 
értékét a 256^2, a zöld értékét a 256^1, a piros értékét a 256^0 számmal szorozták 
meg, majd az így kapott szorzatokat összeadták.
Az így kapott képpontkódokat soronként feldolgozták. Minden sor első számát 
érintetlenül hagyták, az összes többi esetén kivonták a számból a sorban előtte 
lévőt. A biztonság kedvéért az újonnan előállt számsoron még négyszer elvégezték 
ezt a műveletet.

## Követelmények
Nincs más teendőd, mint a ```Decoder``` osztály ```Process``` metódusát 
elkészíteni. A szükséges típusokat megtalálod a ```core``` library ```types/picture.h``` 
és ```types/map.h``` header-ében. A feladatot a C++11-es szabványában bevezetett 
thread library segítségével kell megvalósítanod. Ügyelj arra, hogy a kód tömör és 
olvasható legyen. Próbáld használni az STL által elérhető adatszerkezeteket és 
algoritmusokat.  
A megoldásod dokumentációval kell alátámasztani. Itt a probléma megközelítését és 
megvalósítását is le kell írnod. Ezen felül a program sebességének lokális 
tesztelését is szeretnénk látni. Itt leírhatod, hogy milyen módosítások milyen 
sebességbeli következménnyel jártak.  

## Tesztelés
A megoldásod te is tudod ellenőrizni, a környezet tartalmazza a teszteket.
**Fontos**, hogy az első fordításkor legyen internetkapcsolatod, mert le 
kell töltened a googletest library-t.
Ha minden zöld, akkor sikerült jól megoldanod a feladatot. 

### Docker
1. Indítsd el a docker service-t.  
2. ```docker run -it -v <mappa-név>:/home/student registry.gitlab.com/orsi-demonstrators/17182-environment/test:decoder```  
   ( A mappa-név legyen a teljes elérési útvonal. )  
3. ```git clone -b release_decoder https://gitlab.com/orsi-demonstrators/17182-environment```  
3. ```cd 17182-environment/build```  
4. ```cmake ../src```
5. ```make decoder_unittest```  
6. ```./bin/decoder_unittest```  

### Windows  
1. Navigálj a klónozott git repository-ba.
2. ```mkdir build_vs```
3. ```cd build_vs```
4. ```cmake ..\src```
5. Nyisd meg a ```Project.sln``` solution-t.  
6. Itt tudod szerkeszteni a fájlokat és aztán a Docker környezetben fordítani.  

## Futásidő
A funkcionalitás mellett fontos a gyorsaság is. A környezet tartalmaz egy 
programot, mellyel tudod tesztelni a futási időt is.
A program futása után kiírja a te függvényed futási idejét. Ezt fogjuk összevetni a saját szekvenciális megoldásunkkal.

### Docker
1. Indítsd el a docker service-t.  
2. ```docker run -it -v <mappa-név>:/home/student registry.gitlab.com/orsi-demonstrators/17182-environment/test:decoder```  
   ( A mappa-név legyen a teljes elérési útvonal. )  
3. ```git clone -b release_decoder https://gitlab.com/orsi-demonstrators/17182-environment```  
3. ```cd 17182-environment/build```  
4. ```cmake ../src```
5. ```make decoder_measurer```  
6. ```sequential_decoder```  
   ( A mi szekvenciális megoldásunk futási ideje. )
7. ```./bin/decoder_measurer```  

### Windows  
0. Nyiss egy parancssor-t.
1. Navigálj a klónozott git repository-ba.
2. ```mkdir build_vs```
3. ```cd build_vs```
4. ```cmake ..\src```
5. Nyisd meg a ```Project.sln``` solution-t.  
6. Itt tudod szerkeszteni a fájlokat és aztán a Docker környezetben fordítani.  
