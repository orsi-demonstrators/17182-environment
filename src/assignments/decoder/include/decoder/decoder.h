#pragma once

#include <core/processor.hpp>
#include <core/types/compressed_picture.h>
#include <core/types/picture.h>

struct DECODER : PROCESSOR<COMPRESSED_PICTURE, PICTURE>
{
	virtual PICTURE Process(const COMPRESSED_PICTURE&) override;
	std::vector<PIXEL> ProcessRow(const std::vector<COMPRESSED_PIXEL>&);
};
