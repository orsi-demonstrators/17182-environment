#include "decoder.h"

#include <algorithm>
#include <future>

PICTURE DECODER::Process(const COMPRESSED_PICTURE& aPicture)
{
#ifdef PARALLEL
	const auto Functor = [&](const std::vector<COMPRESSED_PIXEL>& aRow)
	{
		return ProcessRow(aRow);
	};

	std::vector<std::future<std::vector<PIXEL>>> FutureRows;
	FutureRows.reserve(aPicture.size());
	std::transform(aPicture.begin(), aPicture.end(), std::back_inserter(FutureRows),
		[&Functor](const std::vector<COMPRESSED_PIXEL>& Row)
	{
		return std::async(std::launch::async, Functor, Row);
	});

	PICTURE Result;
	Result.reserve(FutureRows.size());
	std::transform(FutureRows.begin(), FutureRows.end(), std::back_inserter(Result),
		[](std::future<std::vector<PIXEL>>& FutureRow)
	{
		return FutureRow.get();
	});
#else
	PICTURE Result;
	Result.reserve(aPicture.size());
	std::transform(aPicture.begin(), aPicture.end(), std::back_inserter(Result),
		[&](const std::vector<COMPRESSED_PIXEL>& Row)
	{
		return ProcessRow(Row);
	});
#endif // PARALLEL
	return Result;
}

std::vector<PIXEL> DECODER::ProcessRow(const std::vector<COMPRESSED_PIXEL>& aRow)
{
	std::vector<PIXEL> Result(aRow.size());

	for (uint32_t Index = aRow.size() - 1; Index > 0; Index--)
	{
		const auto Prev = (Index > 0 ? aRow[Index - 1] : 0);
		const auto PrevPrev = (Index > 1 ? aRow[Index - 2] : 0);
		const auto PrevPrevPrev = (Index > 2 ? aRow[Index - 3] : 0);
		const auto PrevPrevPrevPrev = (Index > 3 ? aRow[Index - 4] : 0);
		const auto PrevPrevPrevPrevPrev = (Index > 4 ? aRow[Index - 5] : 0);

		const auto Res = aRow[Index] + 5 * Prev + 10 * PrevPrev + 10 * PrevPrevPrev + 5 * PrevPrevPrevPrev + PrevPrevPrevPrevPrev;

		const auto Red = static_cast<uint8_t>(Res);
		const auto Green = static_cast<uint8_t>(Res / 256);
		const auto Blue = static_cast<uint8_t>(Res / (256 * 256));
		Result[Index] = { { Red, Green, Blue } };
	}

	const auto Res = aRow[0];
	const auto Red = static_cast<uint8_t>(Res);
	const auto Green = static_cast<uint8_t>(Res / 256);
	const auto Blue = static_cast<uint8_t>(Res / (256 * 256));
	Result[0] = { { Red, Green, Blue } };

	return Result;
}
