#pragma once

#include <cstdint>
#include <vector>

using COORDINATE = std::pair<uint32_t, uint32_t>;
using REGIONS = std::pair<std::vector<uint32_t>, uint32_t>;
