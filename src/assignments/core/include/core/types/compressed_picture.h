#pragma once

#include <iostream>
#include <cstdint>
#include <vector>

using COMPRESSED_PIXEL = int32_t;
using COMPRESSED_PICTURE = std::vector<std::vector<COMPRESSED_PIXEL>>;
