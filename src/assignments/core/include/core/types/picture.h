#pragma once

#include <array>
#include <cstdint>
#include <vector>

using PIXEL = std::array<uint8_t, 3>;
using PICTURE = std::vector<std::vector<PIXEL>>;
