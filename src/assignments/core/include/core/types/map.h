#pragma once

#include <cstdint>
#include <vector>

enum class FIELD : uint8_t
{
	SEA,
	COAST,
	SHORE,
	FOREST,
	DESERT,
	MOUNTAIN,
	MARSH,
	PIRATE_HARBOR,
	TREASURE
};
using MAP = std::vector<std::vector<FIELD>>;
