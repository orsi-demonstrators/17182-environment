#pragma once

template <typename FROM_TYPE, typename TO_TYPE>
struct PROCESSOR
{
	using INPUT = FROM_TYPE;
	using OUTPUT = TO_TYPE;
	
	virtual ~PROCESSOR() = default;
	
	virtual OUTPUT Process(const INPUT&) = 0;
};
