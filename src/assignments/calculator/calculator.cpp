#include "calculator.h"


std::pair<COORDINATE, std::set<std::pair<uint32_t, COORDINATE>>> CALCULATOR::FindWaypoints(const MAP& aMap)
{
	COORDINATE Treasure;
	std::set<std::pair<uint32_t, COORDINATE>> Harbors;
	uint32_t Id = 0;

	for (uint32_t RowIndex = 0; RowIndex < aMap.size(); ++RowIndex)
	{
		for (uint32_t ColIndex = 0; ColIndex < aMap[RowIndex].size(); ++ColIndex)
		{
			if (aMap[RowIndex][ColIndex] == FIELD::TREASURE)
			{
				Treasure = { RowIndex, ColIndex };
			}
			else if (aMap[RowIndex][ColIndex] == FIELD::PIRATE_HARBOR)
			{
				Harbors.insert({ Id++,{ RowIndex, ColIndex } });
			}
		}
	}

	return { Treasure, Harbors };
}


uint32_t CALCULATOR::GetCost(const MAP& aMap, COORDINATE aPosition)
{
	switch (aMap.at(aPosition.first).at(aPosition.second))
	{
	case FIELD::SEA:
		return 1;
	case FIELD::COAST:
		return 5;
	case FIELD::SHORE:
		return 15;
	case FIELD::FOREST:
		return 30;
	case FIELD::DESERT:
		return 500;
	case FIELD::MOUNTAIN:
		return 999999;
	case FIELD::MARSH:
		return 50;
	case FIELD::PIRATE_HARBOR:
		return 0;
	case FIELD::TREASURE:
		return 0;
	default:
		return 999999;
	}
}


bool CALCULATOR::IsValid(const MAP& aMap, COORDINATE aPosition)
{

	switch (aMap.at(aPosition.first).at(aPosition.second))
	{
	case FIELD::MOUNTAIN:
	case FIELD::PIRATE_HARBOR:
		return false;
	default:
		return true;
	}
}


std::vector<COORDINATE> CALCULATOR::GetNeighbours(const MAP& aMap, COORDINATE aPosition)
{
	std::vector<COORDINATE> Result;

	if (aPosition.first > 0)
	{
		Result.push_back({ aPosition.first - 1, aPosition.second });
	}
	if (aPosition.first < aMap.size() - 1)
	{
		Result.push_back({ aPosition.first + 1, aPosition.second });
	}
	if (aPosition.second > 0)
	{
		Result.push_back({ aPosition.first, aPosition.second - 1 });
	}
	if (aPosition.second < aMap[aPosition.first].size() - 1)
	{
		Result.push_back({ aPosition.first, aPosition.second + 1 });
	}

	return Result;
}


std::vector<uint32_t> CALCULATOR::GetRegionAreas(const VALUE_MAP& aRegionMap, uint32_t aRegionCount)
{
	std::vector<uint32_t> Areas(aRegionCount, 0);

	for (uint32_t RowIndex = 0; RowIndex < aRegionMap.size(); ++RowIndex)
	{
		for (uint32_t ColIndex = 0; ColIndex < aRegionMap[RowIndex].size(); ++ColIndex)
		{
			if (aRegionMap[RowIndex][ColIndex] < aRegionCount)
			{
				++Areas[aRegionMap[RowIndex][ColIndex]];
			}
		}
	}

	return Areas;
}


REGIONS CALCULATOR::Process(const MAP& aMap)
{
	COORDINATE Treasure;
	std::set<std::pair<uint32_t, COORDINATE>> Harbors;
	std::tie(Treasure, Harbors) = FindWaypoints(aMap);
	uint32_t HarborCount = Harbors.size();

	VALUE_MAP RegionMap(aMap.size(), std::vector<uint32_t>(aMap[0].size(), UINT32_MAX));
	VALUE_MAP CostMap(aMap.size(), std::vector<uint32_t>(aMap[0].size(), UINT32_MAX));

	for (auto& IdHarbor : Harbors)
	{
		PATH_QUEUE Q(path_cmp);

		Q.push({ 0, IdHarbor.second });
		while (!Q.empty())
		{
			uint32_t CurrCost;
			COORDINATE CurrCoord;
			std::tie(CurrCost, CurrCoord) = Q.top();
			Q.pop();

			if (CurrCost >= CostMap[CurrCoord.first][CurrCoord.second])
				continue;

			CostMap[CurrCoord.first][CurrCoord.second] = CurrCost;
			RegionMap[CurrCoord.first][CurrCoord.second] = IdHarbor.first;

			for (auto NeigCoord : GetNeighbours(aMap, CurrCoord))
			{
				if (!IsValid(aMap, NeigCoord) || RegionMap[NeigCoord.first][NeigCoord.second] == IdHarbor.first)
					continue;
				uint32_t NeigCost = CurrCost + GetCost(aMap, NeigCoord);
				if (CostMap[NeigCoord.first][NeigCoord.second] <= NeigCost)
					continue;
				Q.push({ NeigCost, NeigCoord });
			}
		}
	}

	return { GetRegionAreas(RegionMap, HarborCount), RegionMap.at(Treasure.first).at(Treasure.second) };
}
