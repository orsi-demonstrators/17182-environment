#pragma once

#include <core/processor.hpp>
#include <core/types/map.h>
#include <core/types/regions.h>

#include <map>
#include <queue>
#include <set>


using VALUE_MAP = std::vector<std::vector<uint32_t>>;

using PATH_QUEUE_ELEM = std::pair<uint32_t, COORDINATE>;
auto path_cmp = [](const PATH_QUEUE_ELEM& left, const PATH_QUEUE_ELEM& right) {
	return left.first > right.first;
};
using PATH_QUEUE = std::priority_queue<PATH_QUEUE_ELEM, std::vector<PATH_QUEUE_ELEM>, decltype(path_cmp)>;


struct CALCULATOR : PROCESSOR<MAP, REGIONS>
{
	virtual REGIONS Process(const MAP&) override;

	std::pair<COORDINATE, std::set<std::pair<uint32_t, COORDINATE>>> FindWaypoints(const MAP& aMap);
	uint32_t GetCost(const MAP& aMap, COORDINATE aPosition);
	bool IsValid(const MAP& aMap, COORDINATE aPosition);
	std::vector<COORDINATE> GetNeighbours(const MAP& aMap, COORDINATE aPosition);
	std::vector<uint32_t> GetRegionAreas(const VALUE_MAP& aRegionMap, uint32_t aRegionCount);
};
