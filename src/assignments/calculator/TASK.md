## Történet
A kalózok szövetséget kötöttek, és meg akarják alapítani az első demokratikus kalózállamot, melyben majdnem minden kalóz egyenlő. Az egyes kalózcsapatok az általuk irányított régiók területe alapján kapnak mandátumokat, továbbá, ha nem tudnak dűlőre jutni, a kincs birtokosáé lesz az utolsó szó. Mivel eddig sem hagytad cserben őket, neked adták ezt a feladatot is, cserébe nem veled fogják letisztítani a hajógerincet. Szerencsére a legutóbbi fosztogatás során hozzád került egy algo jegyzet foszlány, melyben két gráfalgoritmus és a hozzájuk tartozó helyességbizonyítások szerepelnek. A helyességbizonyítások túl hosszúak, a határidő pedig túl rövid ahhoz, hogy átnézd őket, de mivel mindkettő végén egy-egy pipát látsz, így elhiszed, hogy ezek jól működnek.
```python
from typing import List, Tuple
from queue import Queue, PriorityQueue


def szelessegi_bejaras(map: List[List[int]], start: Tuple[int, int]) -> List[List[int]]:
    # A map-pel megegyező méretű, inf-eket tartalmazó tömb.
    costs = [[float('inf') for y in range(len(map[0]))] for x in range(len(map))]
    q = Queue()
    
    q.put((0, start))
    while not q.empty():
        curr_cost, curr_coord = q.get()
        
        x, y = curr_coord
        if curr_cost >= costs[x][y]:
            continue
    
        costs[x][y] = curr_cost
        
        for neig_coord in get_neighbours(map, curr_coord):
            neig_cost = curr_cost + get_cost(map, neig_coord)
            n_x, n_y = neig_coord
            if neig_cost >= costs[n_x][n_y]:
                continue
            q.put((neig_cost, neig_coord))
    return costs


def dijkstra(map: List[List[int]], start: Tuple[int, int]) -> List[List[int]]:
    # A map-pel megegyező méretű, inf-eket tartalmazó tömb.
    costs = [[float('inf') for y in range(len(map[0]))] for x in range(len(map))]
    q = PriorityQueue()  # költség, majd koordináta szerint rendezve
    p = set()
    
    q.put((0, start))
    while not q.empty():
        curr_cost, curr_coord = q.get()
        
        if curr_coord in p:
            continue
            
        p.add(curr_coord)
        x, y = curr_coord
        costs[x][y] = curr_cost
        
        for neig_coord in get_neighbours(map, curr_coord):
            if neig_coord in p:
                continue
            neig_cost = curr_cost + get_cost(map, neig_coord)
            q.put((neig_cost, neig_coord))
    return costs
```
Így már nem is olyan reménytelen a helyzet, ugye? 

## Feladat
A feladatod tehát, hogy megadd az egyes kalózokhoz tartozó régiók méretét és a kincs tulajdonosát. Ehhez a térkép minden pontjához meg kell határoznod, hogy az kihez tartozik (tehát, hogy ki tud oda a leggyorsabban, a legkisebb költséggel eljutni). Segítségül elmondták, hogy az egyes tájakon milyen sebességgel tudnak haladni, vagyis mennyi idő alatt teszik meg azt a részt:

- **mély víz (1)**: Mert kalózhajó
- **sekély víz (5)**: Row, row, row your boat. Gently down the stream~
- **part (15)**: Tudnak sétálni
- **erdő (30)**: Az erdő lakóval harcolni kell
- **sivatag (500)**: A homokviharban nem tudnak haladni
- **hegy (999999)**: Még mindig nincs hegymászó felszerelésük
- **mocsár (50)**: A féllábú társakat mindig ki kell húzni

Van két speciális eset is:

- **kalózkikötő**: Innen indul minden kaland
- **kincs**: Végtelen rum

Ha egy ponthoz esetleg több kalóz is ugyanannyi idő alatt tud eljutni, akkor azt a kisebb azonosítóval rendelkező kapja. A kalózok nem túl kreatívak, így egymást számokkal azonosítják, az azonosítókat pedig a térképen való pozíciójuk szerint, sor (x koordináta), majd oszlop (y koordináta) alapján rendezve határozták meg. Az azonosítók **0**-tól kezdődnek! Ezek után a kincs tulajdonosának meghatározása sem okozhat gondot.

## Megjegyzés
A feladatot legegyszerűbben konkurensen lehet megoldani, de a Stack Overflow-s kalandozásaid során megtudtad, hogy párhuzamosan lehet a legnagyobb gyorsulást elérni. Viszont ehhez drasztikusan módosítani kell az algoritmust. 

Ha konkurensen szeretnéd megoldani a feladatot, akkor legegyszerűbb, ha az egyes mezőkhöz vezető leggyorsabb út költségét, valamint a tulajdonos azonosítóját egy-egy 64 bites egészben összefűzve tárolod el. Ez két 32 bites szám esetén a bit shift és a bit vagy operátorokkal egyszerűen megoldható. Továbbá nézz utána az ```std::atomic_uint64_t (std::atomic<std::uint64_t>)``` atomi típusnak.

Ha mégis külön-külön szeretnéd tárolni őket, akkor figyelj az atomicitás megtartására (egyszerre kell olvasnod és módosítanod őket).

## Követelmények
Nincs más teendőd, mint a ```CALCULATOR``` osztály ```Process``` metódusát elkészíteni. A szükséges típusokat megtalálod a ```core``` library ```types.h``` header-ében. A feladatot a C++11-es szabványában bevezetett thread library segítségével kell megvalósítanod. Ügyelj arra, hogy a kód tömör és olvasható legyen. Próbáld használni az STL-beli adatszerkezeteket és algoritmusokat.  

## Tesztelés
A megoldásod te is tudod ellenőrizni, a környezet tartalmazza a teszteket.
**Fontos**, hogy az első fordításkor legyen internetkapcsolatod, mert le 
kell töltened a googletest libraryt.
Ha minden zöld, akkor sikerült jól megoldanod a feladatot. 

### Docker
1. Indítsd el a docker service-t.  
2. ```docker run -it -v <mappa-név>:/home/student registry.gitlab.com/orsi-demonstrators/17182-environment/test:calculator```  
   ( A mappa-név legyen a teljes elérési útvonal. )  
3. ```git clone -b release_calculator https://gitlab.com/orsi-demonstrators/17182-environment```  
3. ```cd 17182-environment/build```  
4. ```cmake ../src```
5. ```make calculator_unittest```  
6. ```./bin/calculator_unittest```  

### Windows  
1. Navigálj a klónozott git repository-ba.
2. ```mkdir build_vs```
3. ```cd build_vs```
4. ```cmake ..\src```
5. Nyisd meg a ```Project.sln``` solution-t.  
6. Itt tudod szerkeszteni a fájlokat és aztán a Docker környezetben fordítani.  

### Atlasz
1. ```git clone -b release_calculator https://gitlab.com/orsi-demonstrators/17182-environment```  
2. ```cd 17182-environment/build```  
3. ```cmake ../src -DATLASZ=1```
4. ```make calculator_unittest```  
5. ```./bin/calculator_unittest```  

## Futásidő
A funkcionalitás mellett fontos a gyorsaság is. A környezet tartalmaz egy 
programot, mellyel tudod tesztelni a futási időt is.
A program futása után kiírja a te függvényed futási idejét. Ezt fogjuk összevetni a saját szekvenciális megoldásunkkal.

### Docker
1. Indítsd el a docker service-t.  
2. ```docker run -it -v <mappa-név>:/home/student registry.gitlab.com/orsi-demonstrators/17182-environment/test:calculator```  
   ( A mappa-név legyen a teljes elérési útvonal. )  
3. ```git clone -b release_calculator https://gitlab.com/orsi-demonstrators/17182-environment```  
3. ```cd 17182-environment/build```  
4. ```cmake ../src```
5. ```make calculator_measurer```  
6. ```sequential_calculator```  
   ( A mi szekvenciális megoldásunk futási ideje. )
7. ```./bin/calculator_measurer```  

### Windows  
0. Nyiss egy parancssor-t.
1. Navigálj a klónozott git repositoryba.
2. ```mkdir build_vs```
3. ```cd build_vs```
4. ```cmake ..\src```
5. Nyisd meg a ```Project.sln``` solutiont.  
6. Itt tudod szerkeszteni, és aztán a Docker környezetben fordítani a fájlokat.  

### Atlasz
1. ```git clone -b release_calculator https://gitlab.com/orsi-demonstrators/17182-environment```  
2. ```cd 17182-environment/build```  
3. ```cmake ../src -DATLASZ=1```
4. ```make calculator_measurer```  
5. ```./bin/calculator_measurer```  
