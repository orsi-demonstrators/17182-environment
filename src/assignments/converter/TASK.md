## Történet
A kalózok nagyon örültek a színes képeknek, azonban rájöttek, 
hogy egyikük sem ért a domborzati térképekhez.
A feladatod most az, hogy a képet átalakítsd a kalózok számára érthető formába.

## Feladat
Arra kértek, hogy a térképen csak azt jelöld, hogy az adott egység 
mégis milyen kategóriába tartozik.
Gyorsan meg is állapították, hogy számukra a következő típusok fontosak:  
- **mély víz (#0000FF)**: Itt a hajóval gyorsan tudnak haladni  
- **sekély víz (#00FFFF)**: Ide már csak csónakkal tudnak bemerészkedni  
- **part (#7CFC00)**: Már sétálnak  
- **erdő (#228B22)**: Megnehezíti a természet az életüket  
- **sivatag (#F4A460)**: Ide csak a nagy kincs reményében mennek  
- **hegy (#708090)**: Mivel kalózok így nem tudják megmászni  
- **mocsár (#8B4513)**: Itt a féllábú kalózok folyton elsüllyednek  
- **kalózkikötő (#000000)**: Innen indul minden kaland  
- **kincs (#FF0000)**: Ezért dolgozol  

Minden képponthoz meg kell határoznod, hogy mit is jelent.
Ehhez először korrigálnod kell a térkép hibáit, minden képpont esetén 
vedd az 5x5-ös környezetét, és átlagold az eredeti képben lévő értékeket. 
A széleken a környezet képen kívül eső pozícióiban a kép hozzájuk legközelebbi
pontjával számolj.
Ezután a képponthoz az átlaghoz színben legközelebb eső információt rendeld.

## Követelmények
Nincs más teendőd, mint a ```CONVERTER``` osztály ```Process```  metódusát 
elkészíteni. A szükséges típusokat megtalálod a ```core``` library ```types/picture.h``` és ```types/map.h``` 
headerjében. A feladatot a C++11-es szabványában bevezetett thread library 
segítségével kell megvalósítanod. Ügyelj arra, hogy a kód tömör és olvasható 
legyen. Próbáld használni az STL által elérhető adatszerkezeteket és algoritmusokat.  
A megoldásod dokumentációval kell alátámasztani. Itt a probléma megközelítését és 
ezenfelül megvalósítását is le kell írnod. Ezen felül a program sebességének lokális 
tesztelését is szeretnénk látni. Itt leírhatod, hogy milyen módosítások milyen 
sebességbeli következménnyel jártak.  

## Tesztelés
A megoldásod te is tudod ellenőrizni, a környezet tartalmazza a teszteket.
**Fontos**, hogy az első fordításkor legyen internetkapcsolatod, mert le 
kell töltened a googletest library-t.
Ha minden zöld, akkor sikerült jól megoldanod a feladatot. 

### Docker
1. Indítsd el a docker service-t.  
2. ```docker run -it -v <mappa-név>:/home/student registry.gitlab.com/orsi-demonstrators/17182-environment/test:converter```  
   ( A mappa-név legyen a teljes elérési útvonal. )  
3. ```git clone -b release_converter https://gitlab.com/orsi-demonstrators/17182-environment```  
3. ```cd 17182-environment/build```  
4. ```cmake ../src```
5. ```make converter_unittest```  
6. ```./bin/converter_unittest```  

### Windows  
1. Navigálj a klónozott git repository-ba.
2. ```mkdir build_vs```
3. ```cd build_vs```
4. ```cmake ..\src```
5. Nyisd meg a ```Project.sln``` solution-t.  
6. Itt tudod szerkeszteni a fájlokat és aztán a Docker környezetben fordítani.  

## Futásidő
A funkcionalitás mellett fontos a gyorsaság is. A környezet tartalmaz egy 
programot, mellyel tudod tesztelni a futási időt is.
A program futása után kiírja a te függvényed futási idejét. Ezt fogjuk összevetni a saját szekvenciális megoldásunkkal.

### Docker
1. Indítsd el a docker service-t.  
2. ```docker run -it -v <mappa-név>:/home/student registry.gitlab.com/orsi-demonstrators/17182-environment/test:converter```  
   ( A mappa-név legyen a teljes elérési útvonal. )  
3. ```git clone -b release_converter https://gitlab.com/orsi-demonstrators/17182-environment```  
3. ```cd 17182-environment/build```  
4. ```cmake ../src```
5. ```make converter_measurer```  
6. ```sequential_converter```  
   ( A mi szekvenciális megoldásunk futási ideje. )
7. ```./bin/converter_measurer```  

### Windows  
0. Nyiss egy parancssor-t.
1. Navigálj a klónozott git repository-ba.
2. ```mkdir build_vs```
3. ```cd build_vs```
4. ```cmake ..\src```
5. Nyisd meg a ```Project.sln``` solution-t.  
6. Itt tudod szerkeszteni a fájlokat és aztán a Docker környezetben fordítani.  
