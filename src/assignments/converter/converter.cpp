#include "converter.h"

#include <map>
#include <future>

MAP CONVERTER::Process(const PICTURE& aPicture)
{
#ifdef PARALLEL
	return ParallelProcess(aPicture);
#endif // PARALLEL

	MAP ConvertedMap;

	for (uint32_t RowIndex = 0; RowIndex < aPicture.size(); ++RowIndex)
	{
		ConvertedMap.push_back(ProcessRow(aPicture, RowIndex));
	}

	return ConvertedMap;
}

MAP CONVERTER::ParallelProcess(const PICTURE& aPicture)
{
	std::vector<std::future<std::vector<FIELD>>> Results;
	Results.reserve(aPicture.size());

	for (uint32_t RowIndex = 0; RowIndex < aPicture.size(); ++RowIndex)
	{
		Results.push_back(std::async(std::launch::async, &CONVERTER::ProcessRow, this, std::ref(aPicture), RowIndex));
	}

	MAP ConvertedMap;
	ConvertedMap.reserve(aPicture.size());
	for (auto& Result : Results)
	{
		ConvertedMap.push_back(Result.get());
	}
	return ConvertedMap;
}

std::vector<FIELD> CONVERTER::ProcessRow(const PICTURE& aPicture, const uint32_t aRowIndex)
{
	std::vector<FIELD> MapRow(aPicture[aRowIndex].size());
	for (uint32_t ColIndex = 0; ColIndex < aPicture[aRowIndex].size(); ++ColIndex)
	{
		const auto Avg = Average(aPicture, aRowIndex, ColIndex, 5, 5);
		const auto Result = Convert(Avg);
		MapRow[ColIndex] = Result;
	}
	return MapRow;
}

PIXEL CONVERTER::Average(const PICTURE& aPicture, uint32_t aRow, uint32_t Col, uint32_t RowSpan, uint32_t ColSpan)
{
	const auto PixelCount = RowSpan * ColSpan;

	std::array<uint32_t, 3> LargePixel{ { 0, 0, 0 } };
	for (int64_t RowIndex = static_cast<int64_t>(aRow) - (RowSpan / 2);
		RowIndex <= static_cast<int64_t>(aRow) + (RowSpan / 2);
		++RowIndex)
	{
		const auto RealRowIndex = (RowIndex < 0) ? 0 : (static_cast<uint32_t>(RowIndex) < aPicture.size() ? RowIndex : aPicture.size() - 1);
		const auto& Row = aPicture[static_cast<unsigned int>(RealRowIndex)];

		for (int64_t ColIndex = static_cast<int64_t>(Col) - (ColSpan / 2);
			ColIndex <= static_cast<int64_t>(Col) + (ColSpan / 2);
			++ColIndex)
		{
			const auto RealColIndex = (ColIndex < 0) ? 0 : (static_cast<uint32_t>(ColIndex) < Row.size() ? ColIndex : Row.size() - 1);
			const auto& Pixel = Row[static_cast<unsigned int>(RealColIndex)];

			LargePixel[0] += Pixel[0];
			LargePixel[1] += Pixel[1];
			LargePixel[2] += Pixel[2];
		}
	}

	return
	{{
		static_cast<uint8_t>(LargePixel[0] / PixelCount),
		static_cast<uint8_t>(LargePixel[1] / PixelCount),
		static_cast<uint8_t>(LargePixel[2] / PixelCount)
	}};
}

FIELD CONVERTER::Convert(const PIXEL& aPixel)
{
	const static std::map<PIXEL, FIELD> ColorConverter =
	{
		{{{ 0x00, 0x00, 0xFF }}, FIELD::SEA},
		{{{ 0x00, 0xFF, 0xFF }}, FIELD::COAST},
		{{{ 0x7C, 0xFC, 0x00 }}, FIELD::SHORE},
		{{{ 0x22, 0x8B, 0x22 }}, FIELD::FOREST},
		{{{ 0xF4, 0xA4, 0x60 }}, FIELD::DESERT},
		{{{ 0x70, 0x80, 0x90 }}, FIELD::MOUNTAIN},
		{{{ 0x8B, 0x45, 0x13 }}, FIELD::MARSH},
		{{{ 0x00, 0x00, 0x00 }}, FIELD::PIRATE_HARBOR},
		{{{ 0xFF, 0x00, 0x00 }}, FIELD::TREASURE}
	};

	const auto PixelDistance = [&](PIXEL Other)
	{
		const auto FirstDiff = static_cast<int32_t>(aPixel[0]) - static_cast<int32_t>(Other[0]);
		const auto SecondDiff = static_cast<int32_t>(aPixel[1]) - static_cast<int32_t>(Other[1]);
		const auto ThirdDiff = static_cast<int32_t>(aPixel[2]) - static_cast<int32_t>(Other[2]);

		return FirstDiff * FirstDiff + SecondDiff * SecondDiff + ThirdDiff * ThirdDiff;
	};

	auto MinDistance = PixelDistance(ColorConverter.begin()->first);
	auto Result = ColorConverter.begin()->second;

	for (const auto& ColorField : ColorConverter)
	{
		const auto Distance = PixelDistance(ColorField.first);

		if (Distance < MinDistance)
		{
			MinDistance = Distance;
			Result = ColorField.second;
		}
	}

	return Result;
}
