#pragma once

#include <core/processor.hpp>
#include <core/types/picture.h>
#include <core/types/map.h>

struct CONVERTER : PROCESSOR<PICTURE, MAP>
{
	virtual MAP Process(const PICTURE&) override;
	MAP ParallelProcess(const PICTURE&);

	std::vector<FIELD> ProcessRow(const PICTURE& aPicture, const uint32_t aRowIndex);
	PIXEL Average(const PICTURE&, uint32_t, uint32_t, uint32_t, uint32_t);
	FIELD Convert(const PIXEL& aPixel);
};
