## Történet
A kalózok elkezdték használni az általad írt programokat, azonban hamar elfáradtak
a számítógép előtt.
Alkut ajánlanak a szabadságodért. ( Komolyan azt hitted, hogy szabad vagy? )
Az előzőleg elkészített programokat össze kell úgy kapcsolnod, hogy rengeteg kódolt
térképhez meg tudja mondani a legrövidebb utat.

## Feladat
Az utolsó feladatod, hogy egy adatcsatornára épülő programot készíts. A feldolgozó 
egységek az előző beadandókban elkészített programok lesznek, újra fel kell használnod őket.
Most a bemeneted több kódolt térkép lesz, az elvárt eredmény pedig az összeshez 
a kalózok ajánlott útvonala.

## Követelmények
Nincs más teendőd, mint a ```PIPELINE``` osztály ```Process``` metódusát és a 
szükséges egységeket ( ```decoder_unit```, ```converter_unit```, ```calculator_unit``` ) 
elkészítsd. Használd fel az előző beadandók megoldását az egységet elkészítéséhez. 
A feladatot a PVM library segítségével kell megvalósítanod. Ügyelj arra, hogy a kód 
tömör és olvasható legyen. Próbáld használni az STL által elérhető adatszerkezeteket 
és algoritmusokat.  

## Tesztelés
A megoldásod te is tudod ellenőrizni, a környezet tartalmazza a teszteket.
**Fontos**, hogy az első fordításkor legyen internetkapcsolatod, mert le 
kell töltened a googletest library-t.
Ha minden zöld, akkor sikerült jól megoldanod a feladatot. 

### Docker
1. Indítsd el a docker service-t.  
2. ```docker run -it -v <mappa-név>:/home/student registry.gitlab.com/orsi-demonstrators/17182-environment/test:pipeline```  
   ( A mappa-név legyen a teljes elérési útvonal. )  
3. ```git clone -b release_pipeline https://gitlab.com/orsi-demonstrators/17182-environment```  
3. ```cd 17182-environment/build```  
4. ```cmake ../src```  
5. ```make pipeline_unittest```  
6. ```printf "* ep=%s\n" $PWD/bin > .hostfile```  
7. ```pvmd .hostfile < /dev/null &``` (pvm daemon elindul)  
8. ```./bin/pipeline_unittest```  
9. ```kill -9 $(pgrep pvmd) > /dev/null``` (pvm daemon lelőve)  

### Windows  
1. Navigálj a klónozott git repository-ba.
2. ```mkdir build_vs```
3. ```cd build_vs```
4. ```cmake ..\src```
5. Nyisd meg a ```Project.sln``` solution-t.  
6. Itt tudod szerkeszteni a fájlokat és aztán a Docker környezetben fordítani.  

### Atlasz
1. ```git clone -b release_pipeline https://gitlab.com/orsi-demonstrators/17182-environment```  
2. ```cd 17182-environment/build```  
3. ```cmake ../src -DATLASZ=1```  
5. ```make pipeline_unittest```  
6. ```printf "* ep=%s\n" $PWD/bin > .hostfile```  
7. ```pvmd .hostfile < /dev/null &``` (pvm daemon elindul)  
8. ```./bin/pipeline_unittest```  
9. ```kill -9 $(pgrep pvmd) > /dev/null``` (pvm daemon lelőve)  

## Futásidő
A funkcionalitás mellett fontos a gyorsaság is. A környezet tartalmaz egy 
programot, mellyel tudod tesztelni a futási időt is.
A program futása után kiírja a te függvényed futási idejét. 
Ezt fogjuk összevetni a saját szekvenciális megoldásunkkal.

### Docker
1. Indítsd el a docker service-t.  
2. ```docker run -it -v <mappa-név>:/home/student registry.gitlab.com/orsi-demonstrators/17182-environment/test:pipeline```  
   ( A mappa-név legyen a teljes elérési útvonal. )  
3. ```git clone -b release_pipeline https://gitlab.com/orsi-demonstrators/17182-environment```  
3. ```cd 17182-environment/build```  
4. ```cmake ../src```
5. ```make pipeline_measurer```  
6. ```printf "* ep=%s\n" $PWD/bin > .hostfile```  
7. ```pvmd .hostfile < /dev/null &``` (pvm daemon elindul)  
8. ```./bin/pipeline_measurer```  
9. ```kill -9 $(pgrep pvmd) > /dev/null``` (pvm daemon lelőve)  
10. ```sequential_pipeline```  
   ( A mi szekvenciális megoldásunk futási ideje. )  

### Windows  
0. Nyiss egy parancssor-t.
1. Navigálj a klónozott git repository-ba.
2. ```mkdir build_vs```
3. ```cd build_vs```
4. ```cmake ..\src```
5. Nyisd meg a ```Project.sln``` solution-t.  
6. Itt tudod szerkeszteni a fájlokat és aztán a Docker környezetben fordítani.  

### Atlasz
1. ```git clone -b release_pipeline https://gitlab.com/orsi-demonstrators/17182-environment```  
2. ```cd 17182-environment/build```  
3. ```cmake ../src -DATLASZ=1```  
5. ```make pipeline_measurer```  
6. ```printf "* ep=%s\n" $PWD/bin > .hostfile```  
7. ```pvmd .hostfile < /dev/null &``` (pvm daemon elindul)  
8. ```./bin/pipeline_measurer```  
9. ```kill -9 $(pgrep pvmd) > /dev/null``` (pvm daemon lelőve)  

