#pragma once

#include <core/processor.hpp>
#include <core/types/compressed_picture.h>
#include <core/types/regions.h>

struct PIPELINE : PROCESSOR<std::vector<COMPRESSED_PICTURE>, std::vector<REGIONS>>
{
	virtual std::vector<REGIONS> Process(const std::vector<COMPRESSED_PICTURE>&) override;
};
