SET(PROJECT_NAME converter_unit)

PROJECT(${PROJECT_NAME})

SET(SOURCES converter_unit.cpp)

ADD_EXECUTABLE(${PROJECT_NAME} ${SOURCES} ${HEADERS})
FIND_PACKAGE(PVM REQUIRED)
TARGET_LINK_LIBRARIES(${PROJECT_NAME} core converter ${PVM_LIBRARY})
TARGET_INCLUDE_DIRECTORIES(${PROJECT_NAME} PRIVATE ${PVM_INCLUDE_DIR})
