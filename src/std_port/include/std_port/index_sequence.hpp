#pragma once

namespace std_port
{
	template <std::size_t ... INTEGERS>
	struct index_sequence
	{
		using type = index_sequence;
		using value_type = std::size_t;
		
		static constexpr std::size_t size() noexcept
		{
			return sizeof...(INTEGERS);
		}
	};
	
	namespace details
	{
		template <class FIRST_SEQUENCE, class SECOND_SEQUENCE>
		struct merge_increase;

		template <std::size_t... FIRST_SEQUENCE, size_t... SECOND_SEQUENCE>
		struct merge_increase<index_sequence<FIRST_SEQUENCE...>, index_sequence<SECOND_SEQUENCE...>> :
			index_sequence<FIRST_SEQUENCE..., (sizeof...(FIRST_SEQUENCE) + SECOND_SEQUENCE)...>
		{};
	}
	
	template <size_t N>
	struct make_index_sequence :
		details::merge_increase<typename make_index_sequence<N/2>::type, typename make_index_sequence<N - N/2>::type>
	{};
	
	template<>
	struct make_index_sequence<0> : index_sequence<>
	{};
	
	template<>
	struct make_index_sequence<1> : index_sequence<0>
	{};
}
