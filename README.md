## Bevezetés
Ez a környezet a 2017-2018-as tanév tavaszi félév beadandóit szolgálja.  

## Fejlesztői környezet

### Windows (csak IDE)
- Telepítsd a ```Visual Studio 2017```-et. ( Ha még nincs fent. )  
- Telepítsd a ```cmake``` build rendszert.

### Docker
- Töltsd le a [Docker](https://www.docker.com/) programot és telepítsd.  
- Ezután indítsd el a docker környezetet és várd meg amíg fut.  
  (Elképzelhető, hogy újra kell indítanod a gépet.)  
- Nyiss meg egy parancssort.  
- ```docker run hello-world```  
  További utasítások az egyes feladatok leírásánál találsz.

### Virtuális gép
- Telepítsd a [VirtualBox](https://www.virtualbox.org/) programot.  
- Tölsd le az [Ubuntu 16.04 LTS](https://www.ubuntu.com/) verziót.
- Készíts egy új virtuális gépet és telepítsd fel benne fenti operációs rendszert.
- Telepítsd a szükséges programokat:
  - ```sudo apt-get update```
  - ```sudo add-apt-repository ppa:ubuntu-toolchain-r/test```
  - ```sudo apt-get update```
  - ```sudo apt-get install -y gcc-7 g++-7 cmake make git pvm-dev```
  - ```sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-7 60```
  - ```sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 60```
- Teszteld le az egyik beadandó lefordításával a környezetet.

## Feladatok  
A feladatok leírását és hozzájuk segítséget az alábbi pontokon találsz:
- [Első feladat](src/assignments/decoder/TASK.md)
- [Második feladat](src/assignments/converter/TASK.md)
- [Harmadik feladat](src/assignments/calculator/TASK.md)
- [Negyedik feladat](src/assignments/pipeline/TASK.md)
